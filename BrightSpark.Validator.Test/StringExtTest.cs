using System;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class StringExtTest
    {
        public class Foo
        {
            public string Name { get; set; }
        }

        [Test]
        public void IsNotNullOrEmpty_Null()
        {
            var validator = new Validator<Foo>()
                .IsNotNullOrEmpty(x => x.Name);

            var model = new Foo();
            var summary = validator.Validate(model);

            Assert.IsFalse(summary.IsValid);
            var res = validator.Results;

            Console.WriteLine("Errors: {0}", res.Count);
        }

        [Test]
        public void IsNotNullOrEmpty_Empty()
        {
            var validator = new Validator<Foo>()
                .IsNotNullOrEmpty(x => x.Name);

            var model = new Foo { Name = string.Empty};
            var summary = validator.Validate(model);

            Assert.IsFalse(summary.IsValid);
            var res = validator.Results;

            Console.WriteLine("Errors: {0}", res.Count);
        }

        [Test]
        public void IsNotNullOrEmpty_NotNull()
        {
            var validator = new Validator<Foo>()
                .IsNotNullOrEmpty(x => x.Name);

            var model = new Foo { Name = "test" };
            var summary = validator.Validate(model);

            Assert.IsTrue(summary.IsValid);
            var res = validator.Results;

            Console.WriteLine("Errors: {0}", res.Count);
        }
    }
}