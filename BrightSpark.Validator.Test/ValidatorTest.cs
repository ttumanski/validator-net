﻿using System;
using System.Linq;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class ValidatorTest
    {
        public class Foo
        {
            public Bar[] Bars { get; set; }

            public string Name { get; set; }

            public bool HasStuff { get; set; }
            public string Stuff { get; set; }

            public bool HasOtherStuff { get; set; }
            public string OtherStuff { get; set; }
        }

        public class Bar
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }

        public class Foo2
        {
            public Bar2[] Bar2s { get; set; }
        }

        public class Bar2
        {
            public Bar TheBar { get; set; }
            public Bar[] Bars { get; set; }
        }

        [Test]
        public void ValidateProperty()
        {
            var barValidator = new Validator<Bar>()
               .Required(x => x.Id, "Id is required")
               .IsNumeric(x => x.Id, "Id must be numeric")
               .Required(x => x.Name, "Name is required");

            var validator = new Validator<Bar2>()
                .Validate(x => x.TheBar, barValidator);


            var model = new Bar2
            {
                TheBar = new Bar { Id = "2a" }
            };


            var summary = validator.Validate(model);
            var res = validator.Results.Values.ToArray();

            Assert.IsFalse(summary.IsValid);

            var expectedKeys = new[]
            {
                "TheBar.Id",
                "TheBar.Name",
            };

            var i = 0;
            foreach (var val in res)
            {
                foreach (var err in val.Errors)
                {
                    Console.WriteLine("{0}:\t{1}", val.Key, err.Error);
                    Assert.AreEqual(expectedKeys[i++], val.Key);
                }
            }

            Assert.AreEqual(2, res.Length);
        }

        [Test]
        public void CollectionValidation()
        {
            var barValidator = new Validator<Bar>()
                .Required(x => x.Id, "Id is required")
                .IsNumeric(x => x.Id, "Id must be numeric")
                .Required(x => x.Name, "Name is required");

            var validator = new Validator<Foo>()
                .ValidCollection(x => x.Bars, barValidator, "Invalid collection");

            var model = new Foo
            {
                Bars = new[]
                {
                    new Bar {Id = "1", Name = "test"},
                    new Bar {Id = "asdf"},
                    new Bar(),
                }
            };

            var summary = validator.Validate(model);
            var res = validator.Results;
            Assert.IsFalse(summary.IsValid);


            foreach (var error in res)
            {
                foreach (var errorText in error.Value.Errors)
                {
                    Console.WriteLine("{0}:{1}", error.Key, errorText.Error);
                }
            }

            Assert.AreEqual(4, res.Count);
        }


        [Test]
        public void MultilevelCollectionValidation()
        {
            var barValidator = new Validator<Bar>()
                .Required(x => x.Id, "Id is required")
                .IsNumeric(x => x.Id, "Id must be numeric")
                .Required(x => x.Name, "Name is required");

            var bar2Validator = new Validator<Bar2>()
                .ValidCollection(x => x.Bars, barValidator);

            var validator = new Validator<Foo2>()
                .ValidCollection(x => x.Bar2s, bar2Validator);

            var model = new Foo2
            {
                Bar2s = new[]
                {
                    new Bar2
                    {
                        Bars = new[]
                        {
                            new Bar {Id = "2", Name = "test3"},
                            new Bar {Id = "asdf"},
                        }
                    },
                    new Bar2
                    {
                        Bars = new[]
                        {
                            new Bar {Id = "1", Name = "test"},
                            new Bar(),
                        }
                    },
                }
            };

            var summary = validator.Validate(model);
            var res = validator.Results.Values.ToArray();

            Assert.IsFalse(summary.IsValid);

            var expectedKeys = new[]
            {
                "Bar2s[0].Bars[1].Id",
                "Bar2s[0].Bars[1].Name",
                "Bar2s[1].Bars[1].Id",
                "Bar2s[1].Bars[1].Name"
            };

            var i = 0;
            foreach (var val in res)
            {
                foreach (var err in val.Errors)
                {
                    Console.WriteLine("{0}:\t{1}", val.Key, err.Error);
                    Assert.AreEqual(expectedKeys[i++], val.Key);
                }
            }

            Assert.AreEqual(4, res.Length);
        }


        [Test]
        public void ErrorCodes()
        {
            var validtor = new Validator<Foo>()
                .Required(x => x.Name, "Name is required", -1);


            var model = new Foo();
            if (!validtor.Validate(model).IsValid)
            {
                foreach (var kvp in validtor.Results)
                {
                    Console.WriteLine("{0}\t{1}\t{2}", kvp.Key, kvp.Value.Errors[0].Error, kvp.Value.Errors[0].Code);
                }

                Assert.AreEqual(1, validtor.Results.Count);
                Assert.AreEqual(-1, validtor.Results["Name"].Errors[0].Code);

                return;
            }
            Assert.Fail();
        }

        [Test]
        public void ValidationExceptionTest()
        {
            var validator = new Validator<Foo>()
                .IsTrue(x => x.Name, x =>
                {
                    return new ValidationResult("O'oh, wrong name!", 123, 0);
                });

            var model = new Foo();
            
            validator.Validate(model);

            Assert.AreEqual(1, validator.Results.Count);
            Assert.AreEqual(1, validator.Results["Name"].Errors.Length);
            Assert.AreEqual("O'oh, wrong name!", validator.Results["Name"].Errors[0].Error);
            Assert.AreEqual(123, validator.Results["Name"].Errors[0].Code);
            Assert.AreEqual(0, validator.Results["Name"].Errors[0].Level);

        }

        [Test]
        public void ConditionalValidationTest()
        {
            var validator = new Validator<Foo>()
                .When(x => x.HasStuff, v => v
                    .Required(x => x.Stuff, "Stuff required", 1000)
                ); 

            var foo = new Foo { HasStuff = true };
            var summary = validator.Validate(foo);

            Assert.IsFalse(summary.IsValid);

            var res = validator.Results;

            Assert.AreEqual(1, res["Stuff"].Errors.Length);
            Assert.AreEqual(1000, res["Stuff"].Errors[0].Code);

        }

        [Test]
        public void NestedConditionalValidationTest()
        {
            var validator = new Validator<Foo>()
                .When(x => x.HasStuff, v => v
                    .Required(x => x.Stuff, "Stuff required", 1000)
                    .When(x => x.HasOtherStuff, v2 => v2
                        .Required(x => x.OtherStuff)
                    )
                );

            var foo = new Foo { HasStuff = false };
            var summary = validator.Validate(foo);

            Assert.IsTrue(summary.IsValid);

            foo.HasStuff = true;
            foo.HasOtherStuff = true;

            summary = validator.Validate(foo);
            Assert.IsFalse(summary.IsValid);

            var res = validator.Results;
            Assert.AreEqual(2, res.Keys.Count);
        }

        [Test]
        public void IsTrueWithValidationResult()
        {
            var validator = new Validator<Foo>()
                .IsTrue(x => x.Name, x =>
                {
                    if (x.Name == "x")
                    {
                        return ValidationResult.OK;
                    }

                    return new ValidationResult("shiiiit");
                });


            var summary  = validator.Validate(new Foo {Name = "y"});
            Assert.IsFalse(summary.IsValid);

            var res = validator.Results;
            foreach (var validationResult in res)
            {
                Console.WriteLine($"validationResult.Key : {string.Join("; ", validationResult.Value.Errors.Select(x => x.Error))}");
            }
        }
    }
}