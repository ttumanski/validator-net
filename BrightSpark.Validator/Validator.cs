﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;

namespace BrightSpark.Validator
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Validator<T> : IValidator<T>
    {
        private const int WarningLevel = -100;

        /// <summary>
        /// 
        /// </summary>
        public static Validator<T> Current { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Validator()
        {
            Current = this;
        }

        /// <summary>
        /// Current error level.
        /// Default is 0 - which is Error. 
        /// Errors are from 0 to -99
        /// Warnings are from -100 to -199
        /// </summary>
        private int _currentLevel;

        private readonly List<ConditionalValidator> _conditionalValidators = new List<ConditionalValidator>();

        /// <summary>
        /// 
        /// </summary>
        private readonly IDictionary<string, ValidationResult> _validationResults = new Dictionary<string, ValidationResult>();

        /// <summary>
        /// 
        /// </summary>
        private readonly IDictionary<string, IList<PropertyValidator<T>>> _propertyValidators = new Dictionary<string, IList<PropertyValidator<T>>>();

        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<string> _excludedFields = new List<string>();

        /// <summary>
        /// If true, all validations are executed.
        /// If false, properties validations will be executed until one fails.
        /// </summary>
        public bool ContinueOnError { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IDictionary<string, ValidationResult> Results => _validationResults;

        /// <summary>
        /// From this point on all property validators will have given level. 
        /// Levels from 0 to -99 are considered as errors.
        /// -99 to -.. are considered as warnings.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public Validator<T> SetLevel(int level)
        {
            _currentLevel = level;
            return this;
        }

        /// <summary>
        /// Register field validator
        /// </summary>
        /// <param name="key"></param>
        /// <param name="validator"></param>
        private void AddPropertyValidator(string key, Func<T, string, ValidationResult> validator)
        {
            if (!_propertyValidators.ContainsKey(key))
            {
                _propertyValidators[key] = new List<PropertyValidator<T>>();
            }

            _propertyValidators[key].Add(new PropertyValidator<T>
            {
                Validator = validator,
            });
        }

        /// <summary>
        /// Register field validation error
        /// </summary>
        /// <param name="key"></param>
        /// <param name="errorMessage"></param>
        /// <param name="level"></param>
        /// <param name="errorCode"></param>
        public Validator<T> AddError(string key, string errorMessage, int level = 0, int? errorCode = null)
        {
            var error = new ValidationError
            {
                Level = level,
                Error = errorMessage,
                Code = errorCode,
            };

            return AddError(key, error);
        }

        public Validator<T> AddError(string key, ValidationError error)
        {
            if (!_validationResults.ContainsKey(key))
            {
                _validationResults[key] = new ValidationResult();
            }

            _validationResults[key].Key = key;
            _validationResults[key].AddError(error);

            return this;
        }

        public Validator<T> AddErrors(string key, IEnumerable<ValidationError> errors)
        {
            if (errors != null)
            {
                foreach (var error in errors)
                {
                    AddError(key, error);
                }
            }

            return this;
        }



        /// <summary>
        /// Register field validation error
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="level"></param>
        /// <param name="errorCode"></param>
        public Validator<T> AddError<TProp>(Expression<Func<T, TProp>> predicate, string errorMessage, int level = 0, int? errorCode = null)
        {
            var key = ValidatorUtil.GetMemberName(predicate);
            return AddError(key, errorMessage, level, errorCode);
        }

        /// <summary>
        /// Returns if key has already validation errors
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool HasErrorAlready(string key)
        {
            // return _validationResults.ContainsKey(key) && _validationResults[key].Errors.Any(x => x.Level == 0);
            return _validationResults.ContainsKey(key);
        }
        
        /// <summary>
        /// Add validator which checks if model property is set (i.e not null)
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode">Error code</param>
        /// <returns></returns>
        public Validator<T> Required<TProp>(Expression<Func<T, TProp>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return Required(memberName, predicate, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property is set (i.e not null)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode">Error code</param>
        /// <returns></returns>
        public Validator<T> Required<TProp>(string key, Expression<Func<T, TProp>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            return IsTrue(key, x =>
            {
                var f = predicate.Compile();
                var value = f(x);
                return value != null;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TProp"></typeparam>
        /// <param name="predicate"></param>
        /// <param name="propertyValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public Validator<T> Validate<TProp>(Expression<Func<T, TProp>> predicate, Validator<TProp> propertyValidator,
            string errorMessage = "*", int? errorCode = null)
        {
            return IsTrue(predicate, (x, rootKey) =>
            {
                var prop = predicate.Compile()(x);

                propertyValidator.Validate(prop);
                
                var res = propertyValidator.Results;
                foreach (var kvp in res)
                {
                    foreach (var validationError in kvp.Value.Errors)
                    {
                        var key = $"{rootKey}.{kvp.Key}";
                        AddError(key, validationError.Error, validationError.Level, validationError.Code);
                    }
                }

                return true;
            }, errorMessage, errorCode);
        } 

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="predicate"></param>
        /// <param name="itemValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public Validator<T> ValidCollection<T1>(Expression<Func<T, IList<T1>>> predicate, Validator<T1> itemValidator, string errorMessage = "*", int? errorCode = null)
        {
            return IsTrue(predicate, (x, rootKey) =>
            {
                IList<T1> collection = predicate.Compile()(x);

                for (var i = 0; i < collection.Count; ++i)
                {
                    if (itemValidator.Validate(collection[i]).IsValid)
                    {
                        continue;
                    }

                    var res = itemValidator.Results;
                    foreach (var kvp in res)
                    {
                        foreach (var validationError in kvp.Value.Errors)
                        {
                            var key = $"{rootKey}[{i}].{kvp.Key}";
                            AddError(key, validationError.Error, validationError.Level, validationError.Code);
                        }
                    }
                }

                return true;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public Validator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, bool> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrue(memberName, customValidator, errorMessage, errorCode);
        }

        public Validator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, ValidationResult> validator)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrue(memberName, validator);
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public Validator<T> IsTrue(string key, Func<T, bool> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            ValidationResult f(T model, string k) => customValidator(model)
                ? ValidationResult.OK
                : new ValidationResult(errorMessage, errorCode, _currentLevel);

            AddPropertyValidator(key, f);
            return this;
        }

        public Validator<T> IsTrue(string key, Func<T, ValidationResult> validator)
        {
            AddPropertyValidator(key, (x, k) => validator(x));
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public Validator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, string, bool> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrue(memberName, customValidator, errorMessage, errorCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public Validator<T> IsTrue(string key, Func<T, string, bool> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            ValidationResult f(T model, string k) => customValidator(model, k)
                ? ValidationResult.OK
                : new ValidationResult(errorMessage, errorCode, _currentLevel);

            AddPropertyValidator(key, f);
            return this;
        }
        
        /// <summary>
        /// Retrievs validation results for given key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ValidationResult ResultsFor(string key)
        {
            if (!_validationResults.ContainsKey(key))
            {
                return new ValidationResult();
            }

            return _validationResults[key];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ValidationSummary Validate(T model)
        {
            IDictionary<string, string> errorMessages;
            return Validate(model, out errorMessages);
        }

        ValidationSummary IValidator.Validate(object model)
        {
            return Validate((T)model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="errorMessages"></param>
        /// <returns></returns>
        [Obsolete("This method should no longer be used, please use Validate(T model) instead. Results property contains all messages and codes.")]
        public ValidationSummary Validate(T model, out IDictionary<string, string> errorMessages)
        {
            IDictionary<string, string> warningMessages;
            return Validate(model, out errorMessages, out warningMessages);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="errorMessages"></param>
        /// <param name="warningMessages"></param>
        /// <returns></returns>
        [Obsolete("This method should no longer be used, please use Validate(T model) instead. Results property contains all messages and codes.")]
        public ValidationSummary Validate(T model, out IDictionary<string, string> errorMessages, out IDictionary<string, string> warningMessages)
        {
            Results.Clear();

            foreach (var conditionalValidator in _conditionalValidators)
            {
                if (conditionalValidator.Condition(model) && !conditionalValidator.Validator.Validate(model).IsValid)
                {
                    MergeResults(conditionalValidator.Validator.Results);
                }
            }

            foreach (var kvp in _propertyValidators)
            {
                var key = kvp.Key;

                if (_excludedFields.Contains(key))
                {
                    continue;
                }

                ValidateProperty(model, key, kvp.Value);
            }

            #region out params

            errorMessages = new Dictionary<string, string>();
            foreach (var kvp in Results)
            {
                var k = kvp.Key;
                foreach (var err in kvp.Value.Errors)
                {
                    if (err.Level > WarningLevel && err.Level <= 0)
                    {
                        errorMessages[k] = err.Error;
                        break;
                    }
                }
            }

            warningMessages = new Dictionary<string, string>();
            foreach (var kvp in Results)
            {
                var k = kvp.Key;
                foreach (var err in kvp.Value.Errors)
                {
                    if (err.Level <= WarningLevel)
                    {
                        warningMessages[k] = err.Error;
                        break;
                    }
                }
            }

            #endregion

            return new ValidationSummary
            {
                IsValid = errorMessages.Count == 0,
                Results = Results
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsValidProperty(T model, string key)
        {
            Results.Clear();

            if (_propertyValidators.ContainsKey(key))
            {
                return true;
            }

            ValidateProperty(model, key, _propertyValidators[key]);

            return !Results.ContainsKey(key);
        }

        private void ValidateProperty(T model, string key, IList<PropertyValidator<T>> propertyValidators)
        {
            foreach (var propertyValidator in propertyValidators)
            {
                if (!ContinueOnError && HasErrorAlready(key))
                {
                    continue;
                }

                //var errorMessage = propertyValidator.ErrorMessage;
                //var level = propertyValidator.Level;
                //var errorCode = propertyValidator.ErrorCode;
                var res = propertyValidator.Validator(model, key);
                AddErrors(key, res.Errors);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excludeFields"></param>
        public void SetExcludedFields(IEnumerable<string> excludeFields)
        {
            _excludedFields = excludeFields;
        }

        private void MergeResults(IDictionary<string, ValidationResult> d2)
        {
            foreach (var kvp in d2)
            {
                if (!_validationResults.ContainsKey(kvp.Key))
                {
                    _validationResults[kvp.Key] = kvp.Value;
                }
                else
                {
                    foreach (var validationError in kvp.Value.Errors)
                    {
                        _validationResults[kvp.Key].AddError(validationError);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Current = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="func"></param>
        /// <param name="validatorSetup"></param>
        /// <returns></returns>
        public Validator<T> When(Func<T, bool> func, Func<Validator<T>, Validator<T>> validatorSetup)
        {
            var validator = new Validator<T>();
            validatorSetup(validator);
            _conditionalValidators.Add(new ConditionalValidator {Condition = func, Validator = validator });
            return this;
        }

        private class ConditionalValidator
        {
            public Func<T, bool> Condition { get; set; }
            public Validator<T> Validator { get; set; }
        }
    }
}