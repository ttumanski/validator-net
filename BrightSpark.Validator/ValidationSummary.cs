using System.Collections.Generic;

namespace BrightSpark.Validator
{
    public class ValidationSummary
    {
        public bool IsValid { get; set; }
        public IDictionary<string, ValidationResult> Results { get; set; }
    }
}