namespace BrightSpark.Validator
{
    public static class ValidationLevel
    {
        public const int Error = 0;
        public const int Warning = -100;
    }
}