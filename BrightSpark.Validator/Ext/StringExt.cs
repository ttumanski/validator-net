﻿using System;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace BrightSpark.Validator
{
    public static class StringExt
    {
        /// <summary>
        /// Add validator which checks if model string property value is not null or empty
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("38CE0120-DA95-43B7-88AB-84C4AE14D09B")]
        public static Validator<T> IsNotNullOrEmpty<T>(this Validator<T> validator,
            Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            var key = ValidatorUtil.GetMemberName(predicate);
            return validator.IsNotNullOrEmpty(key, predicate, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property value is not null or empty.
        /// Key can be different from model property selector.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="validator"></param>
        /// <param name="key"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("67A151CE-E762-4B7B-9F0B-18FC3D5EE77B")]
        public static Validator<T> IsNotNullOrEmpty<T>(this Validator<T> validator, string key,
            Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = predicate.Compile().Invoke(x);
                return value != null && value.Trim() != string.Empty;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if string property value is not null or empty
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("BBC1BC12-4D24-482F-986F-CF3D4C4D44EF")]
        public static Validator<T> IsNotNullOrEmpty<T>(this Validator<T> validator,
            string key, string value, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x => value != null && value.Trim() != string.Empty, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property value is null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("9A3E5E34-3B0D-4D41-942F-336320EC0BBE")]
        public static Validator<T> IsNull<T>(this Validator<T> validator, Expression<Func<T, string>> predicate,
            string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(predicate, x =>
            {
                var valueFunc = predicate.Compile();
                var value = valueFunc(x);
                return value == null;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property length is less than or equal to given min length.
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="minLength"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("3BA2E30A-2E22-4298-B187-1425B4C76774")]
        public static Validator<T> MinLength<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, int minLength, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(predicate, x =>
            {
                var valueFunc = predicate.Compile();
                var value = valueFunc(x);

                return string.IsNullOrEmpty(value) || value.Length >= minLength;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property length is greater than or equal to given length
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="maxLength"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("4EE7994E-791E-48F4-B9D1-01884AB8DE47")]
        public static Validator<T> MaxLength<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, int maxLength, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(predicate, x =>
            {
                var valueFunc = predicate.Compile();
                var value = valueFunc(x);

                return value == null || value.Length <= maxLength;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid e-mail address
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="required"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("56B39353-2E8E-45D3-8A2C-7909DEFF095D")]
        public static Validator<T> IsEmail<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", bool required = false, int? errorCode = null)
        {
            return validator.IsEmail(predicate, x => required, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid e-mail address
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="required"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("22B4C9C2-9967-488A-AFCF-0DF788CBCA2F")]
        public static Validator<T> IsEmail<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, Func<T, bool> required, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(predicate, x =>
            {
                var value = predicate.Compile().Invoke(x);

                var requiredValue = required(x);
                if (string.IsNullOrEmpty(value))
                {
                    // when it is empty value and is 
                    // - required return false
                    // - not required return true (no need to regex)
                    return !requiredValue;
                }

                const string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                        @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                        @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

                var regex = new Regex(strRegex);
                return regex.IsMatch(value);

            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid uri
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="required"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("AA1C6D69-9E4F-44E1-9F55-3907BBB03328")]
        public static Validator<T> IsUri<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, string errorMessage, bool required = false, int? errorCode = null)
        {
            return validator.IsUri(predicate, x => required, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid uri
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="required"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("96803155-7777-4907-AC31-0E4600E3B164")]
        public static Validator<T> IsUri<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, Func<T, bool> required, string errorMessage, int? errorCode = null)
        {
            return validator.IsTrue(predicate, x =>
            {
                var value = predicate.Compile().Invoke(x);

                var requiredValue = required(x);
                if (string.IsNullOrEmpty(value) && requiredValue)
                {
                    return false;
                }

                try
                {
                    new Uri(value);
                    return true;
                }
                catch
                {
                    return false;
                }

            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid date
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("71B656B4-9267-4C4A-99B5-4D66DE6AEAA9")]
        public static Validator<T> IsDate<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, string errorMessage, int? errorCode = null)
        {
            string key = ValidatorUtil.GetMemberName(predicate);
            return validator.IsDate(key, predicate, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid date		
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="key"> </param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("D55EE152-C227-4442-814E-D3BD201EE32A")]
        public static Validator<T> IsDate<T>(this Validator<T> validator, string key, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                DateTime dt;
                var value = predicate.Compile().Invoke(x);
                return value == null || DateTime.TryParse(value, out dt);

            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid integer
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("51306DBC-77F6-4074-B41D-1B213720B020")]
        public static Validator<T> IsInteger<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            string key = ValidatorUtil.GetMemberName(predicate);
            return validator.IsInteger(key, predicate, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid integer
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="key"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("687D06F2-1822-48E4-9C7A-B240CFB58B5A")]
        public static Validator<T> IsInteger<T>(this Validator<T> validator, string key, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                int i;
                var value = predicate.Compile().Invoke(x);
                return value == null || int.TryParse(value, out i);

            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid numeric value
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("8503DAC7-57A0-4F6C-9FA1-833E9C6EF343")]
        public static Validator<T> IsNumeric<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            string key = ValidatorUtil.GetMemberName(predicate);
            return validator.IsNumeric(key, predicate, errorMessage);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid numeric value
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="key"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("3773B497-B6EC-4953-832A-20B519683549")]
        public static Validator<T> IsNumeric<T>(this Validator<T> validator, string key, Expression<Func<T, string>> predicate, string errorMessage, int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                decimal d;
                var value = predicate.Compile().Invoke(x);
                return value == null || decimal.TryParse(value, NumberStyles.Any, null, out d);
            }, errorMessage, errorCode);
        }


        /// <summary>
        /// Add validator which checks if model string property matches given regex pattern
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="pattern"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errCode"></param>
        /// <returns></returns>
        [ValidatorId("83452972-12D2-43EF-9973-B3D226540D8B")]
        public static Validator<T> Regex<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, string pattern, string errorMessage = "*", int? errCode = null)
        {
            return validator.IsTrue(predicate, x =>
            {
                string input = predicate.Compile().Invoke(x);
                return input != null && System.Text.RegularExpressions.Regex.IsMatch(input, pattern);
            }, errorMessage, errCode);
        }

        /// <summary>
        /// Add validator which checks if model string property matches given regex pattern or is null
        /// </summary>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="pattern"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errCode"></param>
        /// <returns></returns>
        [ValidatorId("905E9433-435A-4BEA-B33F-BE68CB65C596")]
        public static Validator<T> RegexOrNull<T>(this Validator<T> validator, Expression<Func<T, string>> predicate, string pattern, string errorMessage = "*", int? errCode = null)
        {
            return validator.IsTrue(predicate, x =>
            {
                string input = predicate.Compile().Invoke(x);
                return input == null || System.Text.RegularExpressions.Regex.IsMatch(input, pattern);
            }, errorMessage, errCode);
        }
    }
}
