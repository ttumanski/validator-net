﻿using System;
using System.Linq.Expressions;

namespace BrightSpark.Validator
{
    public static class ComparableExt
    {
        /// <summary>
        /// Add validator which checks if model property value is greater than given item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="item"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("857B3DA7-BC6B-4876-A434-0B62045BBA6F")]
        public static Validator<T> IsGreaterThan<T, T1>(this Validator<T> validator, Expression<Func<T, T1>> predicate,
            T1 item, string errorMessage = "*", int? errorCode = null) where T1 : IComparable<T1>
        {
            return validator.IsTrue(predicate, x =>
            {
                var compareResult = predicate.Compile().Invoke(x).CompareTo(item);
                return compareResult > 0;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is greater than or equal to given item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="item"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("739D1376-6771-457A-B8FA-76A2D847AC47")]
        public static Validator<T> IsGreaterThanOrEqualTo<T, T1>(this Validator<T> validator, Expression<Func<T, T1>> predicate,
            T1 item, string errorMessage = "*", int? errorCode = null) where T1 : IComparable<T1>
        {
            return validator.IsTrue(predicate, x =>
            {
                var compareResult = predicate.Compile().Invoke(x).CompareTo(item);
                return compareResult >= 0;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is less than given item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="item"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("B205C931-E405-4EE3-8418-D5199BBD82B1")]
        public static Validator<T> IsLessThan<T, T1>(this Validator<T> validator, Expression<Func<T, T1>> predicate,
            T1 item, string errorMessage = "*", int? errorCode = null) where T1 : IComparable<T1>
        {
            return validator.IsTrue(predicate, x =>
            {
                var compareResult = predicate.Compile().Invoke(x).CompareTo(item);
                return compareResult < 0;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is less than or equal to given item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="item"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("030C4056-5CF7-431C-B1DE-0C26B2A14EB2")]
        public static Validator<T> IsLessThanOrEqualTo<T, T1>(this Validator<T> validator, Expression<Func<T, T1>> predicate,
            T1 item, string errorMessage = "*", int? errorCode = null) where T1 : IComparable<T1>
        {
            return validator.IsTrue(predicate, x =>
            {
                var compareResult = predicate.Compile().Invoke(x).CompareTo(item);
                return compareResult <= 0;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is equal to given item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T1"></typeparam>
        /// <param name="validator"></param>
        /// <param name="predicate"></param>
        /// <param name="item"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        [ValidatorId("B15290B6-D500-4AFE-AD07-C0040686F0F4")]
        public static Validator<T> IsEqualTo<T, T1>(this Validator<T> validator, Expression<Func<T, T1>> predicate,
            T1 item, string errorMessage = "*", int? errorCode = null) where T1 : IComparable<T1>
        {
            return validator.IsTrue(predicate, x =>
            {
                var compareResult = predicate.Compile().Invoke(x).CompareTo(item);
                return compareResult == 0;
            }, errorMessage, errorCode);
        }
    }
}
