﻿using System;

namespace BrightSpark.Validator
{
    public interface IValidator : IDisposable
    {
        ValidationSummary Validate(object model);
    }

    public interface IValidator<T> : IValidator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ValidationSummary Validate(T model);
    }
}
