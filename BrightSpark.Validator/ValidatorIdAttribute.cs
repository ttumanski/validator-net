﻿using System;

namespace BrightSpark.Validator
{
    public class ValidatorIdAttribute : Attribute
    {
        public Guid Uid { get; private set; }

        public ValidatorIdAttribute(string uid)
        {
            Uid = new Guid(uid);
        }
    }
}