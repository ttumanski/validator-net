﻿using System.Linq.Expressions;

namespace BrightSpark.Validator
{
    internal static class ValidatorUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static string GetMemberName(LambdaExpression predicate)
        {
            var s = predicate.ToString();
            var i = s.IndexOf('.');
            return s.Substring(i + 1).TrimEnd(')');
        }
    }
}
