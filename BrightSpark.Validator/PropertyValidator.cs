﻿using System;

namespace BrightSpark.Validator
{
    public class PropertyValidator<T>
    {
        public Func<T, string, ValidationResult> Validator { get; set; }
    }
}
