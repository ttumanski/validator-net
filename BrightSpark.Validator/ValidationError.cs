namespace BrightSpark.Validator
{
    public class ValidationError
    {
        public int Level { get; set; }
        public string Error { get; set; }
        public int? Code { get; set; }

        public ValidationError()
        {
            
        }

        public ValidationError(string error, int? code = null, int? level = null)
        {
            Error = error;
            Level = level ?? 0;
            Code = code;
        }
    }
}