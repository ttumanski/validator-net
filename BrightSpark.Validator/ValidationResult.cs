using System.Collections.Generic;

namespace BrightSpark.Validator
{
    public class ValidationResult
    {
        private readonly List<ValidationError> _errors = new List<ValidationError>();
        public static readonly ValidationResult OK = new ValidationResult();

        public string Key { get; set; }
        public ValidationError[] Errors => _errors.ToArray();

        public ValidationResult()
        {
        }

        public ValidationResult(ValidationError error)
        {
            AddError(error);
        }

        public ValidationResult(string error, int? code = null, int? level = null)
        {
            AddError(error, code, level);
        }

        public void AddError(string error, int? code = null, int? level = null)
        {
            var validationError = new ValidationError(error, code, level);
            AddError(validationError);
        }

        public void AddError(ValidationError error)
        {
            _errors.Add(error);
        }
    }
}