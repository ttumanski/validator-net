# Validator.NET



## Features

* Fluent
* Property validation
* Collection validation
* Conditional validation

### Validating properties

```
#!c#

var validator = new Validator<Foo>()
    .IsNotNullOrEmpty(x => x.Name);
```

List of bundled validations:

* IsGreaterThan
* IsGreaterThanOrEqualTo
* IsLessThan
* IsLessThanOrEqualTo
* IsEqualTo
* IsNotNullOrEmpty
* IsNull
* MinLength
* MaxLength
* IsEmail
* IsUri
* IsDate
* IsInteger
* IsNumeric
* Regex
* RegexOrNull

### Validating collections
```
#!c#

public class Foo
{
    public Bar[] Bars { get; set; }
}

public class Bar
{
    public string Id { get; set; }
    public string Name { get; set; }
}

```

```
#!c#

var barValidator = new Validator<Bar>()
    .Required(x => x.Id, "Id is required")
    .IsNumeric(x => x.Id, "Id must be numeric")
    .Required(x => x.Name, "Name is required");

var validator = new Validator<Foo>()
    .ValidCollection(x => x.Bars, barValidator, "Invalid collection");

var model = new Foo
{
    Bars = new[]
    {
        new Bar {Id = "1", Name = "test"},
        new Bar {Id = "asdf"},
        new Bar(),
    }
};

var isValid = validator.IsValid(model);
var res = validator.Results;
```

### Conditional validation


```
#!c#

public class Foo
{
    public bool HasStuff { get; set; }
    public string Stuff { get; set; }

    public bool HasOtherStuff { get; set; }
    public string OtherStuff { get; set; }
}
```


```
#!c#

var validator = new Validator<Foo>()
    .When(x => x.HasStuff, v => v
        .Required(x => x.Stuff, "Stuff required")
        .When(x => x.HasOtherStuff, v2 => v2
            .Required(x => x.OtherStuff, "Other stuff is also required")
        )
    );

```